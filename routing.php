<?php

class Routing
{
    function processRequest()
    {
        global $LOGGER;
        switch($_SERVER['REQUEST_METHOD'])
        {
            case 'GET':
                $this->processGet();
                break;
            case 'POST':
                $this->processPost();
                break;
        }
    }

    function processGet()
    {
        global $UTILS;
        global $LOGGER;
        $parameters = [];
        $halves = explode('?', $_SERVER['REQUEST_URI']);
        if(count($halves) > 2)
        {
            $UTILS->return500();
            return;
        }
        else if(count($halves) == 2)
        {
            $paramsets = explode('&', $halves[1]);
            foreach($paramsets as $paramset)
            {
                $keys = explode('=', $paramset);
                $parameters[$keys[0]] = (isset($keys[1]) ? $keys[1] : null);
            }
        }

        switch($halves[0])
        {
            case '/GreenRoute/':
                include 'REST/accounts.php';
                //$ACCOUNT->userLogin('gaylele@lele.le', 'lele');
                if ($ACCOUNT->getSessionData()["COOKIE"] == null) {
                    header('Location: /GreenRoute/login');
                } else {
                    include ('VIEWS/home.php');
                }
                break;
            case '/test/GreenRoute/':
                //include 'REST/accounts.php';
                $LOGGER->logToClient('100', 'this is a test today');
                //$ACCOUNT->sendResetPasswordEmail('alx.ionescu97@gmail.com');
                break;
//            case '/card/download':
//                include 'REST/files.php';
//                $FILE->downloadFile($parameters['filename']);
//                break;
            case '/GreenRoute/logout':
                include 'REST/accounts.php';
                $ACCOUNT->userLogout();
                header("Location: /GreenRoute/");
                break;
            case '/GreenRoute/login':
                include 'REST/accounts.php';
                if ($ACCOUNT->getSessionData()["COOKIE"] == null) {
                    include('VIEWS/login.php');
                } else {
                    header("Location: /GreenRoute/");
                }
                break;
            case '/GreenRoute/info':
                phpinfo();
                break;
        }
    }

    function processPost()
    {
        global $LOGGER;
//        $parameters = [];
//        $paramsets = explode('&', file_get_contents('php://input'));
//
//        foreach($paramsets as $paramset)
//        {
//            $keys = explode('=', $paramset);
//            $parameters[$keys[0]] = (isset($keys[1]) ? $keys[1] : null);
//        }
        $parameters = json_decode(file_get_contents('php://input'), true);

        switch($_SERVER['REQUEST_URI'])
        {
            case '/GreenRoute/test':
                break;
            case '/GreenRoute/user/register':
                include 'REST/accounts.php';
                $ACCOUNT->userRegister($_POST['email'], $_POST['password']);
                $ACCOUNT->userLogin($_POST['email'], $_POST['password']);
                header("Location: /GreenRoute/");
                break;
            case '/GreenRoute/user/login':
                include 'REST/accounts.php';
                $ACCOUNT->userLogin($_POST['email'], $_POST['password']);
                header("Location: /GreenRoute/");
                break;
            case '/GreenRoute/user/delete':
                include 'REST/accounts.php';
                $ACCOUNT->userDelete();
                break;
            case '/GreenRoute/user/resetpass':
                include 'REST/accounts.php';
                $ACCOUNT->resetPass($parameters['token'], $parameters['password']);
                break;
            case '/GreenRoute/user/sendmail':
                include 'REST/accounts.php';
                $ACCOUNT->sendResetPasswordEmail($parameters['email']);
                break;
        }
    }
}

$ROUTE = new Routing();
