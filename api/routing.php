<?php

class Routing
{
    function processRequest()
    {
        global $LOGGER;
        switch($_SERVER['REQUEST_METHOD'])
        {
            case 'GET':
                $this->processGet();
                break;
            case 'POST':
                $this->processPost();
                break;
        }
    }

    function processGet()
    {
        global $UTILS;
        global $LOGGER;
        $parameters = [];
        $halves = explode('?', $_SERVER['REQUEST_URI']);
        if(count($halves) > 2)
        {
            $UTILS->return500();
            return;
        }
        else if(count($halves) == 2)
        {
            $paramsets = explode('&', $halves[1]);
            foreach($paramsets as $paramset)
            {
                $keys = explode('=', $paramset);
                $parameters[$keys[0]] = (isset($keys[1]) ? $keys[1] : null);
            }
        }

        switch($halves[0])
        {
            case '/GreenRoute/api/':
                include 'REST/accounts.php';
                //$ACCOUNT->userLogin('gaylele@lele.le', 'lele');
                if ($ACCOUNT->getSessionData()["COOKIE"] == null) {
                    header('Location: login');
                } else {
                    include ('VIEWS/home.php');
                }
                break;
            case '/test/GreenRoute/api/':
                //include 'REST/accounts.php';
                $LOGGER->logToClient('100', 'this is a test today');
                //$ACCOUNT->sendResetPasswordEmail('alx.ionescu97@gmail.com');
                break;
//            case '/card/download':
//                include 'REST/files.php';
//                $FILE->downloadFile($parameters['filename']);
//                break;
            case '/GreenRoute/api/login':
                include('VIEWS/login.php');
                break;
            case '/GreenRoute/api/info':
                phpinfo();
                break;
            case '/GreenRoute/api/aqi':
                include 'REST/aqi.php';
                $AQI->getAroundLocation($parameters['lat'], $parameters['lng'], $parameters['radius']);
                break;
        }
    }

    function processPost()
    {
        global $LOGGER;
//        $parameters = [];
//        $paramsets = explode('&', file_get_contents('php://input'));
//
//        foreach($paramsets as $paramset)
//        {
//            $keys = explode('=', $paramset);
//            $parameters[$keys[0]] = (isset($keys[1]) ? $keys[1] : null);
//        }
        $parameters = json_decode(file_get_contents('php://input'), true);

        switch($_SERVER['REQUEST_URI'])
        {
            case '/GreenRoute/api/test':
                break;
            case '/GreenRoute/api/user/register':
                include 'REST/accounts.php';
                $LOGGER->setOutputJSON(true);
                $ACCOUNT->userRegister($parameters['email'], $parameters['password']);
                break;
            case '/GreenRoute/api/user/login':
                include 'REST/accounts.php';
                $LOGGER->setOutputJSON(true);
                $ACCOUNT->userLogin($parameters['email'], $parameters['password']);
                break;
            case '/GreenRoute/api/user/logout':
                include 'REST/accounts.php';
                $LOGGER->setOutputJSON(true);
                $ACCOUNT->userLogout();
                break;
            case '/GreenRoute/api/user/delete':
                include 'REST/accounts.php';
                $LOGGER->setOutputJSON(true);
                $ACCOUNT->userDelete();
                break;
            case '/GreenRoute/api/user/resetpass':
                include 'REST/accounts.php';
                $LOGGER->setOutputJSON(true);
                $ACCOUNT->resetPass($parameters['token'], $parameters['password']);
                break;
            case '/GreenRoute/api/user/sendmail':
                include 'REST/accounts.php';
                $LOGGER->setOutputJSON(true);
                $ACCOUNT->sendResetPasswordEmail($parameters['email']);
                break;
            case '/GreenRoute/api/aqi':
                include 'REST/aqi.php';
                $AQI->getAroundLocation($parameters['lat'], $parameters['lng'], $parameters['radius']);
                break;
        }
    }
}

$ROUTE = new Routing();
