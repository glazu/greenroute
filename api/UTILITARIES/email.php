<?php

class Emailer {
    private $HTML_CODE = "";

    function sendEmail($email, $token) {
        global $LOGGER;

        $myFile = '/var/www/html/api/UTILITARIES/resetpassword.html';
        if (!file_exists($myFile)) {
            $LOGGER->logToClient('101', 'File not found');
        } else if(!$handle = fopen($myFile, 'r')) {
            $LOGGER->logToClient('101', 'Can\'t open file');
        } else {
            $message = fread($handle, filesize($myFile));

            // To send HTML mail, the Content-type header must be set
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Reply-To: '. $email . "\r\n" ;
            $headers .= 'X-Mailer: PHP/' . phpversion() . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $headers .= 'From: BCReader <recovery@bcreaderapp.com>' . "\r\n";

            $body = str_replace('{{action_url}}', 'http://web.bcreaderapp.com/resetpassword/?token=' . $token, $message);

            $LOGGER->logToServer('609', $email);
            $LOGGER->logToServer('609', $token);

            $res = mail($email, 'Password reset', $body, $headers);
            if($res) {
                $LOGGER->logToClient('609', '');
            } else {
                $LOGGER->logToClient('611', '');
            }
        }
    }
}
$EMAILER = new Emailer();
