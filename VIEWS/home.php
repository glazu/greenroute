<!DOCTYPE html>
<html>
<head>
    <title>Home - GreenRoute</title>
    <!-- Core stylesheets -->
    <link href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/GreenRoute/assets/css/bootstrap-dark.min.css" rel="stylesheet" type="text/css">
    <link href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/GreenRoute/assets/css/pixeladmin-dark.min.css" rel="stylesheet" type="text/css">
    <link href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/GreenRoute/assets/css/widgets.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin" rel="stylesheet" type="text/css">
    <link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css">
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <!-- Theme -->
    <link href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/GreenRoute/assets/css/themes/mint-dark.min.css" rel="stylesheet" type="text/css">

    <!-- Pace.js -->
    <script src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/GreenRoute/assets/pace/pace.min.js"></script>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 100%;
        }
        /* Optional: Makes the sample page fill the window. */
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }

        #wrapper {
            position: relative;
        }

        #sidebar {
            top: 50px;
            left: 1%;
            position: absolute;
            z-index: 99!important;
            width: 25%;
        }
    </style>
</head>
<body>
<div id="wrapper">
    <div id="sidebar" class="panel">
        <div class="panel-title">Surrounding AQ data</div>
        <small class="panel-subtitle text-muted">Air Quality data about your surroundings.</small>
        <div class="panel-body">
            <div class="input-group m-b-2">
                <input class="form-control" id="from-form" placeholder="Start here" type="text" style="margin-bottom:1px;">
                <span class="input-group-btn">
                <button type="button" id="from-button" class="btn"><i class="fa fa-map-marker"></i></button>
              </span>
            </div>
            <div class="input-group m-b-2">
                <input class="form-control" id="to-form" placeholder="End here" type="text" style="margin-bottom:1px;">
                <span class="input-group-btn">
                <button type="button" id="to-button" class="btn"><i class="fa fa-map-marker"></i></button>
              </span>
            </div>
            <button type="button" id="form-go" class="btn btn-lg btn-primary btn-block">Search</button>
            <br/>
            <div id="aqi-parent" class="panel panel-dark panel-body-colorful widget-profile widget-profile-centered">
                <div class="panel-body">
                    <div id="aqi" style="text-align: center; font-size:3em;"></div>
                </div>
            </div>
            <br/>
            <div id="pollution-data">
                <div class="col-xs-12 col-sm-6">
                    <div class="panel box">
                        <div class="box-row">
                            <div class="box-cell p-x-2 p-y-1 bg-black text-xs-center font-size-11 font-weight-semibold">
                                CO<sub>2</sub>
                            </div>
                        </div>
                        <div class="box-row">
                            <div class="box-cell p-y-2">
                                <div class="easy-pie-chart" data-suffix="ppm" id="local-pollution-CO2"><span class="font-size-14 font-weight-light"></span></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="panel box">
                        <div class="box-row">
                            <div class="box-cell p-x-2 p-y-1 bg-black text-xs-center font-size-11 font-weight-semibold">
                                NO<sub>2</sub>
                            </div>
                        </div>
                        <div class="box-row">
                            <div class="box-cell p-y-2">
                                <div class="easy-pie-chart" data-suffix="ppm" id="local-pollution-NO2"><span class="font-size-14 font-weight-light"></span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="map"></div>
<!-- jQuery -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/GreenRoute/assets/js/greenroute.js"></script>
<script>
    var map;
    function initMap() {
        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;
        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 44.4293478, lng: 26.0798005},
            zoom: 13
        });
        directionsDisplay.setMap(map);
        $("#form-go").on('click', function () {
            calculateAndDisplayRoute(directionsService, directionsDisplay);
        });

        function calculateAndDisplayRoute(directionsService, directionsDisplay) {
            var from = document.getElementById('from-form').value;
            var to = document.getElementById('to-form').value;
            directionsService.route({
                origin: from.substring(1, from.length-1),
                destination: to.substring(1, to.length-1),
                travelMode: 'DRIVING',
                provideRouteAlternatives: true
            },function (response, status) {
                    if (status == google.maps.DirectionsStatus.OK) {
                        for (var i = 0, len = response.routes.length; i < len; i++) {
                            new google.maps.DirectionsRenderer({
                                map: map,
                                directions: response,
                                routeIndex: i
                            });
                        }
                    } else {
                        $("#error").append("Unable to retrieve your route<br />");
                    }
                }
            );
        }
        getSurroundingData(44.4293478,26.0798005, 20);
        //getHeatMapData(44.4293478,26.0798005, 50);
        window.setInterval(function() { fetchPollutionData() },1000);
        getMarkers(44.4293478,26.0798005, 500);
    }
    function addMarker(location, map, lbl) {
        // Add the marker at the clicked location, and add the next-available label
        // from the array of alphabetical characters.
        var marker = new google.maps.Marker({
            position: location,
            label: lbl,
            map: map
        });
    }

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDWHsqDipqUOPPVOs8-rpmEzI3QjKTjMVU&callback=initMap&libraries=visualization"
        async defer></script>


<!-- Core scripts -->
<script src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/GreenRoute/assets/js/bootstrap.min.js"></script>
<script src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/GreenRoute/assets/js/pixeladmin.min.js"></script>

<!-- Your scripts -->
<script src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/GreenRoute/assets/js/app.min.js"></script>

</body>
</html>