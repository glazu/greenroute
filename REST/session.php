<?php

class Session
{
    private $YEAR_TO_SECONDS = 31536000;
    private $COOKIE_SESSION_LENGTH = 26;
    private $COOKIE_SESSION_NAME = 'iac_session';
    function getSessionValues()
    {
        $SESSION_COOKIE = (isset($_COOKIE[$this->COOKIE_SESSION_NAME]) ? $_COOKIE[$this->COOKIE_SESSION_NAME] : null);
        $SESSION_USERID = $this->getSession($SESSION_COOKIE);
        $SESSION_VALID = !($SESSION_USERID === NULL);

        return ['COOKIE' => $SESSION_COOKIE, 'USERID' => $SESSION_USERID, 'VALID' => $SESSION_VALID];
    }
    function setSession($userId)
    {
        global $UTILS;
        $token = $this->generateToken();
        $timepy = time() + $this->YEAR_TO_SECONDS;
        $this->insertSession($userId, $token, $UTILS->getSQLTime($timepy));
        $this->sessionCookie($token, $timepy);
    }
    
    function getSession($sesscookie)
    {
        global $DATABASE;
        //$sesscookie = $_COOKIE[$this->COOKIE_SESSION_NAME];
        $rows = $DATABASE->GetRows('sessions', 'sessiontoken=\'' . $sesscookie . '\'');  
        return $rows->fetch_assoc()['userId'];
    }
    
    function insertSession($userId, $session, $time)
    {
        global $DATABASE;

        $DATABASE->InsertRow('sessions', ['sessionToken' => $session, 'userId' => $userId, 'expiresAt' => $time]);
    }
    function updateSession($oldsession, $newsession, $time)
    {
        global $DATABASE;
        $DATABASE->UpdateRow('sessions', ['sessionToken' => $newsession, 'expiresAt' => $time], 'sessionToken=\'' . $oldsession . '\'');
    }
    function remakeSession()
    {
        global $UTILS;
        $sessvalues = $this->getSessionValues();
        $newcookie = $this->generateToken();
        $timepy = time() + $UTILS->YEARS_TO_SECONDS;
        $this->updateSesssion($sessvalues['COOKIE'], $newcookie, $UTILS->getSQLTime($timepy));
        $this->sessionCookie($newcookie, $timepy);
    }
    function sessionCookie($session, $time)
    {
        setcookie($this->COOKIE_SESSION_NAME, $session, $time, '/GreenRoute/');
    }
    function deleteSession($session)
    {
        global $DATABASE;
        $DATABASE->DeleteRow('sessions', 'sessionToken=\'' . $session . '\'');
    }
    public function generateToken() {
        $dummy_token = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        $dummy_token_length = 61;
        $token = '';
        
        for ($i = 0; $i < $this->COOKIE_SESSION_LENGTH; $i++) {
            $token .= $dummy_token[rand(0, $dummy_token_length)];
        }
        return $token;
    }
}

$SESSION = new Session();