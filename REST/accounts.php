<?php

class Account {
    function userExists($email) {
        global $DATABASE;
        $rows = $DATABASE->GetRows('users', 'email = \'' . $email . '\'');
        if($rows->num_rows >= 1) {
            return true;
        } else {
            return false;
        }
    }
    
    function userInsert($email, $hash) {
        global $DATABASE;
        global $LOGGER;
        $result = $DATABASE->InsertRow('users', ['email' => $email, 'password' => $hash]);
        $LOGGER->logToServer('500', 'User insert ' . $email . ' result was ' . $result);
        return $result;
    }
    
    function userRegister($email, $password) {
        global $LOGGER;
        global $UTILS;
        if(!$UTILS->validateEmail($email) || !$UTILS->validatePassword($password)) {
            $LOGGER->logToClient('501', ''); //Send error about invalid password or email
        } else if($this->userExists($email)) { //Send error about email already existing
                $LOGGER->logToClient('502', '');
            } else { //Password/Email valid and not existing.. proceeding to register
                $hash = password_hash($password, PASSWORD_BCRYPT);
                if($this->userInsert($email, $hash)) {
                    $LOGGER->logToClient('500', '');
                } else {
                    $LOGGER->logToClient('503', '');
                }
            }
        //TODO generate email token and send it to the faggot
    }
    
    function userCheck($email, $plain_password) {
        global $DATABASE;
        $found = $DATABASE->GetRows('users', 'email=\'' . $email . '\'');
        if($found->num_rows == 0) {
            return false;
        }
        $row = $found->fetch_assoc();
        if(password_verify($plain_password, $row['password'])) {
            return $row['userId'];
        } else {
            return false;
        }
    }
    
    function userLogin($email, $plain_password) { //Returns user session n shit
        global $LOGGER;
        global $UTILS;
        $userid = $this->userCheck($email, $plain_password);
        include 'REST/session.php';
        $aboutss = $SESSION->getSessionValues();
        if($aboutss['VALID'])
        {
            $LOGGER->logToClient('602', '');
        }
        else if($userid !== false)
        {
            $SESSION->setSession($userid);
            $LOGGER->logToClient('600', '');
            return true;
        } else {
            $LOGGER->logToClient('601', 'Invalid email/password combination');
            return false;
        }
    }
    
    function userLogout(){
        global $LOGGER;
        include 'REST/session.php';
        $aboutss = $SESSION->getSessionValues();
        var_dump($aboutss);
        if($aboutss['VALID']) {
            global $DATABASE;
            $DATABASE->DeleteRow('sessions', 'sessionToken=\'' . $aboutss['COOKIE'] . '\'');
            $SESSION->sessionCookie($aboutss['COOKIE'], time() - 86400);
            $LOGGER->logToClient('603', '');
            return true;
        } else {
            $LOGGER->logToClient('604', '');
            return false;
        }
    }
    
    function userDelete() { //Delete everything about the user (sessions, cards, and login credentials
        global $LOGGER;
        include 'REST/session.php';
        $aboutss = $SESSION->getSessionValues();
        if($aboutss['VALID']) {
            global $DATABASE;
            include 'REST/files.php';
            $DATABASE->DeleteRow('users', 'userId=\'' . $aboutss['USERID'] . '\'');
            $DATABASE->DeleteRow('sessions', 'userId=\'' . $aboutss['USERID'] . '\'');
            $FILE->deleteAllUserCards($aboutss['USERID']);
            $SESSION->sessionCookie('', time() - 86400);
            $LOGGER->logToClient('605', '');
            return true;
        } else {
            $LOGGER->logToClient('606', '');
            return false;
        }
    }
    
    function sendResetPasswordEmail($email) {
        global $DATABASE;
        global $LOGGER;
        $found = $DATABASE->GetRows('users', 'email=\'' . $email . '\'');
        if($found->num_rows == 0) { //user not found
            $LOGGER->logToClient('610', '');
        } else {
            global $UTILS;
            include 'UTILITARIES/email.php';
            
            $token = $UTILS->generateToken(60);
            $DATABASE->UpdateRow('users', ['passwordToken' => $token], 'email=\'' . $email . '\'');
            $EMAILER->sendEmail($email, $token);
        }
    }
    
    function resetPass($token, $newpass) {
        global $LOGGER;
        global $UTILS;
        
        header('Access-Control-Allow-Origin: http://web.bcreaderapp.com');  
        
        if(is_null($token) || empty($token) || $token == '') {
            $LOGGER->logToClient('615', '');
            return;
        }
        
        if(!$UTILS->validatePassword($newpass)) {
            //Send error about empty pass
            $LOGGER->logToClient('612', '');
        } else {
            global $DATABASE;
            $found = $DATABASE->GetRows('users', 'passwordToken=\'' . $token . '\'');
            if($found->num_rows == 1) {
                $hash = password_hash($newpass, PASSWORD_BCRYPT);
                $DATABASE->UpdateRow('users', ['password' => $hash, 'passwordToken' => NULL], 'passwordToken=\'' . $token . '\'');
                $LOGGER->logToClient('614', '');
            } else { //token not found
                $LOGGER->logToClient('613', '');
            }
        }
    }
            
    function getUserType() {
        global $LOGGER;
        include 'REST/session.php';
        $aboutss = $SESSION->getSessionValues();
        if($aboutss['VALID']) {
            global $DATABASE;
            $found = $DATABASE->GetRows('users', 'userId=\'' . $aboutss['USERID'] . '\'');
            if($found->num_rows == 0) {
                return false;
            } else {
                $row = $found->fetch_assoc();
                $LOGGER->logUserType($row['userType']);
            }
        }
    }

    function getSessionData() {
        include 'REST/session.php';
        return $SESSION->getSessionValues();
    }
}

$ACCOUNT = new Account();

